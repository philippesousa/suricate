# Crud React


## System Requirement:

- Java 8
- Docker 
- Node
- MySql server with connection named by surittec, the application will auto create all entity by it self.


## Runtime comands:

- Backend: mvn spring-boot:run
- Frontend: npm start


# Used tecnologies:

- Docker
- Mysql 5.6 +
- Spring
- Hibernate
- Maven
- Auth JWT
- React 
- Redux
- React-Router
- Bootstrap




